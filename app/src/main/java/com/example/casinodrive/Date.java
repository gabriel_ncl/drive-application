package com.example.casinodrive;

import java.text.SimpleDateFormat;

/**
 * Created by NICULAE Gabriel
 */
public class Date {

    String pattern = "dd-MM-yyyy hh:mm:ss:S";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String date = simpleDateFormat.format(new java.util.Date());

}

