package com.example.casinodrive;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Shop extends AppCompatActivity {

    private RecyclerView liste;
    private FirebaseFirestore mFirebaseFirestore;
    private FloatingActionButton mCart;
    private FirestoreRecyclerAdapter adapter;
    private String email = login_activity.getuEmail().getText().toString().trim();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        mCart = findViewById(R.id.cart);
        liste = findViewById(R.id.firestore_list);

        mFirebaseFirestore = FirebaseFirestore.getInstance();

        mCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Shop.this, Cart.class);
                startActivity(intent);
            }
        });

        final Query query = mFirebaseFirestore.collection("Product");

        FirestoreRecyclerOptions<Product> options = new FirestoreRecyclerOptions.Builder<Product>()
                .setQuery(query, Product.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Product, ProductViewHolder>(options) {
            @NonNull
            @Override
            public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_single, parent, false);
                return new ProductViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull ProductViewHolder holder, int position, @NonNull final Product model) {
                holder.list_name.setText(model.getName());
                holder.list_price.setText(model.getPrice() + "€");
                Picasso.get().load(model.getImage()).into(holder.list_image);
                final String name = model.getName();
                final double price = model.getPrice();
                final String image = model.getImage();


                holder.addProductInCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addedToCart(name, price, image);
                    }
                });
            }
        };

        liste.setHasFixedSize(true);
        liste.setLayoutManager(new GridLayoutManager(this, 2));
        liste.setAdapter(adapter);
        Toast.makeText(getApplicationContext(), "Connecté à " + email, Toast.LENGTH_LONG).show();
    }

    private void addedToCart(String name, double price, String image) {

        String pattern = "dd-MM-yyyy hh:mm:ss:S";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Map<String, Object> order = new HashMap<>();
        order.put("name", name);
        order.put("price", price);
        order.put("image", image);
        order.put("date", date);

        mFirebaseFirestore.collection("Order").document(email).collection("order").document(date)
                .set(order)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "Ajout au panier", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Echec", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private class ProductViewHolder extends RecyclerView.ViewHolder {

        private TextView list_name;
        private TextView list_price;
        private ImageView list_image;
        private ImageButton addProductInCart;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            list_name = itemView.findViewById(R.id.itemName);
            list_price = itemView.findViewById(R.id.itemPrice);
            list_image = itemView.findViewById(R.id.itemImage);
            addProductInCart = itemView.findViewById(R.id.addProductInCart);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        adapter.startListening();
        super.onStart();
    }

}