package com.example.casinodrive;

/**
 * Created by NICULAE Gabriel
 */
public class ProductInCart {

    private String name;
    private long price;
    private String image;
    private String date;

    public ProductInCart() {
    }

    public ProductInCart(String name, long price, String image, String date) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
