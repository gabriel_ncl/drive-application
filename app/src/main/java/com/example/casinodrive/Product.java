package com.example.casinodrive;

import com.squareup.picasso.Picasso;

/**
 * Created by NICULAE Gabriel
 */
public class Product {

    private String name;
    private double price;
    private String image;
    private String date;

    public Product() {
    }

    public Product(String name, double price, String image, String date) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
