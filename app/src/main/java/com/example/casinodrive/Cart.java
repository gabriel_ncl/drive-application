package com.example.casinodrive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

public class Cart extends AppCompatActivity {

    private RecyclerView liste;
    private FirebaseFirestore mFirebaseFirestore;
    private FirestoreRecyclerAdapter adapter;
    private String email = login_activity.getuEmail().getText().toString().trim();
    private ImageButton deleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        liste = findViewById(R.id.cart_list);
        deleteBtn = findViewById(R.id.delete);
        mFirebaseFirestore = FirebaseFirestore.getInstance();

        final Query query = mFirebaseFirestore.collection("Order").document(email).collection("order");

        FirestoreRecyclerOptions<ProductInCart> options = new FirestoreRecyclerOptions.Builder<ProductInCart>()
                .setQuery(query, ProductInCart.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<ProductInCart, Cart.ProductViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ProductViewHolder holder, int position, @NonNull ProductInCart model) {
                holder.list_name.setText(model.getName());
                holder.list_price.setText(model.getPrice() + "€");
                Picasso.get().load(model.getImage()).into(holder.list_image);
                final String date = model.getDate();

                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deletedFromCart(date);
                    }
                });

            }

            @NonNull
            @Override
            public Cart.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_single_cart, parent, false);
                return new Cart.ProductViewHolder(view);
            }
        };

        liste.setHasFixedSize(true);
        liste.setLayoutManager(new LinearLayoutManager(this));
        liste.setAdapter(adapter);
    }

    private void deletedFromCart(String date) {

        mFirebaseFirestore.collection("Order").document(email).collection("order").document(date)
                .delete();
    }

    private class ProductViewHolder extends RecyclerView.ViewHolder {

        private TextView list_name;
        private TextView list_price;
        private ImageView list_image;
        private ImageButton deleteBtn;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            list_name = itemView.findViewById(R.id.nameCart);
            list_price = itemView.findViewById(R.id.priceCart);
            list_image = itemView.findViewById(R.id.imageCart);
            deleteBtn = itemView.findViewById(R.id.delete);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        adapter.startListening();
        super.onStart();
    }
}
