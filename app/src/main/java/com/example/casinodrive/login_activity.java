package com.example.casinodrive;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class login_activity extends AppCompatActivity {

    private Button btnSignup, btnLogin;
    private TextInputLayout uEmailWrapper, uPasswordWrapper;
    private static EditText uEmail, uPassword;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnSignup = findViewById(R.id.activity_register);
        btnLogin = findViewById(R.id.activity_connection);

        uEmailWrapper = findViewById(R.id.emailWrapper);
        uPasswordWrapper = findViewById(R.id.passwordWrapper);

        uEmail = findViewById(R.id.email);
        uPassword = findViewById(R.id.password);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login_activity.this, register_activity.class);
                startActivity(intent);
            }
        });
        mAuth = FirebaseAuth.getInstance();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = uEmail.getText().toString().trim();
                final String password = uPassword.getText().toString().trim();

                if (email.isEmpty()){
                    uEmailWrapper.setError("Veuillez entrer un mail");
                    uEmailWrapper.requestFocus();
                    return;
                }
                if (password.isEmpty()){
                    uPasswordWrapper.setError("Veuillez entrer un mot de passe");
                    uPasswordWrapper.requestFocus();
                    return;
                }
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            btnLogin.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(login_activity.this, Shop.class);
                                    startActivity(intent);
                                }
                            });
                        }
                        else{
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
    public static EditText getuEmail() {
        return uEmail;
    }
}
