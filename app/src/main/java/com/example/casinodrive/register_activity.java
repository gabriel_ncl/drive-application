package com.example.casinodrive;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

public class register_activity extends AppCompatActivity {

    private EditText ufirstName, ulastName, uemail, upassword, uphone;
    private TextInputLayout ufirstNameWrapper, ulastNameWrapper, uemailWrapper, upasswordWrapper, uphoneWrapper;
    private Button btnRegister;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        ufirstName = findViewById(R.id.RegisterFirstName);
        ulastName = findViewById(R.id.RegisterLastName);
        uemail = findViewById(R.id.RegisterEmail);
        upassword = findViewById(R.id.RegisterPassword);
        uphone = findViewById(R.id.RegisterPhone);

        ufirstNameWrapper = findViewById(R.id.registerFirstNameWrapper);
        ulastNameWrapper = findViewById(R.id.registerLastNameWrapper);
        uemailWrapper = findViewById(R.id.registerEmailWrapper);
        upasswordWrapper = findViewById(R.id.RegisterPasswordWrapper);
        uphoneWrapper = findViewById(R.id.RegisterPhoneWrapper);

        btnRegister = findViewById(R.id.activity_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser() != null) {
                    Intent intent = new Intent(register_activity.this, login_activity.class);
                    startActivity(intent);
                }

                final String firstname = ufirstName.getText().toString().trim();
                final String lastname = ulastName.getText().toString().trim();
                final String email = uemail.getText().toString().trim();
                final String password = upassword.getText().toString().trim();
                final String phone = uphone.getText().toString().trim();

                if (firstname.isEmpty()) {
                    ufirstNameWrapper.setError("Entrez un Prénom");
                    ufirstNameWrapper.requestFocus();
                    return;
                }


                if (lastname.isEmpty()) {
                    ulastNameWrapper.setError("Entrez un Nom");
                    ulastNameWrapper.requestFocus();
                    return;
                }

                if (email.isEmpty()) {
                    uemailWrapper.setError("Entrez un Email");
                    uemailWrapper.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    upasswordWrapper.setError("Entrez un Mot de passe");
                    upasswordWrapper.requestFocus();
                    return;
                }

                if (phone.isEmpty()) {
                    uphoneWrapper.setError("Entrez un numéro de téléphone");
                    uphoneWrapper.requestFocus();
                    return;
                }
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            final User user = new User(firstname, lastname, email, phone);
                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(register_activity.this, "Utilisateur ajouté", Toast.LENGTH_LONG).show();
                                    }
                                    else{
                                        Toast.makeText(register_activity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(register_activity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
